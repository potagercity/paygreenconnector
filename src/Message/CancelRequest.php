<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Edenred\Message;


class CancelRequest extends AbstractRequest
{
    public function getData()
    {
        $this->validate('transactionReference');
        $this->validate('amount');

        $data = [];

        $amount = $this->amountToCents($this->getAmount());

        $data['amount'] = $amount;

        return $data;
    }

    public function getEndpoint()
    {
        return $this->endpoint . '/transactions/' . $this->getTransactionReference() . '/actions/cancel';
    }

    public function getHttpMethod()
    {
        return 'POST';
    }
}
