<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Edenred\Message;


class PurchaseRequest extends AuthorizeRequest
{

    public function getData()
    {
        $data = parent::getData();

        unset($data['capture_mode']);

        return $data;
    }


}
