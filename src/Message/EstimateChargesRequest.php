<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Edenred\Message;


class EstimateChargesRequest extends AbstractRequest
{
    public function getData()
    {
        $this->validate('userLogin');
        $this->validate('amount');

        $data = [];

        $amount = $this->amountToCents($this->getAmount());

        $data['amount'] = $amount;

        return $data;
    }

    public function getEndpoint()
    {
        return $this->endpoint . '/users/' . $this->getUserLogin() . '/actions/estimate-charge';
    }

    public function getHttpMethod()
    {
        return 'POST';
    }

    public function setUserLogin($value)
    {
        return $this->setParameter('userLogin', $value);
    }

    public function getUserLogin()
    {
        return $this->getParameter('userLogin');
    }
}
