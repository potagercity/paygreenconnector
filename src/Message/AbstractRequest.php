<?php

namespace Potagercity\Edenred\Message;

abstract class AbstractRequest extends \Omnipay\Common\Message\AbstractRequest
{
    /**
     *
     *
     * @var string URL
     */
    protected $endpoint = '';

    abstract public function getEndpoint();


    abstract public function getHttpMethod();

    /**
     * @return array
     */
    public function getHeaders()
    {
        $headers = array();

        if ($this->getClientId()) {
            $headers['X-Client-id'] = $this->getClientId();
        }

        if ($this->getClientSecret()) {
            $headers['X-Client-secret'] = $this->getClientSecret();
        }

        $headers['Content-Type'] = "application/json";

        return $headers;
    }

    /**
     * {@inheritdoc}
     */
    public function sendData($data)
    {
        $headers = array_merge(
            $this->getHeaders(),
            array('Authorization' => 'Bearer ' . $this->getAccessToken())
        );

        $body = $data ? json_encode($data) : null;

        $url = $this->getServerUrl() . $this->getEndpoint();
        $httpResponse = $this->httpClient->request(
            $this->getHttpMethod(),
            $url,
            $headers,
            $body
        );

        return $this->createResponse($httpResponse->getBody()->getContents(), $httpResponse->getHeaders());
    }

    protected function createResponse($data, $headers = [])
    {
        return $this->response = new Response($this, $data, $headers);
    }


    public function setClientId($value)
    {
        return $this->setParameter('clientId', $value);
    }

    public function getClientId()
    {
        return ($this->getParameter('clientId'));
    }

    public function setClientSecret($value)
    {
        return $this->setParameter('clientSecret', $value);
    }

    public function getClientSecret()
    {
        return ($this->getParameter('clientSecret'));
    }

    public function setServerUrl($value)
    {
        return $this->setParameter('serverUrl', $value);
    }

    public function getServerUrl()
    {
        return $this->getParameter('serverUrl');
    }


    public function setAccessToken($value)
    {
        return $this->setParameter('accessToken', $value);
    }

    public function getAccessToken()
    {
        return ($this->getParameter('accessToken'));
    }

    protected function amountToCents($amount)
    {
        return intval(number_format(($amount * 100), 0, '.', ''));
    }
}
