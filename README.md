# Omnipay: Edenred

**Paygreen driver for the Omnipay PHP payment processing library**

## Installation

Omnipay is installed via [Composer](http://getcomposer.org/). To install, simply require `league/omnipay` and `potagercity/paygreen` with Composer:

```
composer require league/omnipay omnipay/paygreen
```



